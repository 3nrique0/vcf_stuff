#! /usr/bin/python3
# Author: Enrique Ortega-Abboud


# Count masked and missing data from VCF files.



#############################
## Tasks: CORE TASKS DONE

## [*] Read file line by line -> Low impact on read I/O and low memory usage
## [*] Skip commented lines
## [*] Split by \t
## [*] print nb individuals (nb columns - 9)  & nb snps (nb lines w/o preambule
## [*] Counters:
##        [*] by line (locus or site) = Frequencies: (missing data)/(nb ind), (masked data)/(nb ind) 
##        [*]by column = Make 2 lists with the lenght of nb individuals, +1 at the position where there
##        will be a missing or a masked -each in it's own list

## [*] Output copied .lmiss from VCFtools 
##      [*] CHR     POS     N_DATA  N_GENOTYPE_FILTERED     N_MISS  F_MISS    N_MASK  F_MASK
##          - CHR and POS are taken from the VCF
##          - N_DATA is the number of 'GT spaces' for each sample.
##              It is NOT the real GT number, it'd change depening on the ploidy
##          - N_GENOTYPE_FILTERED : I don't know how this information is taken, the value is fixed to 0
##          - N_MISS, N_MASK are the counts
##          - F_MISS, F_MASK are the Frequencies (couts/N_DATA). 
##              In this case the ploidy is factored out, frequencies are the same as VCFtools

## [*] Output copied from .imiss from VCFtools
##    INDV    N_DATA  N_GENOTYPES_FILTERED    N_MISS  F_MISS

## + Unknow variables are written as '.'
## + The lenght of the examples given is shorter compared to the ones you might find
## + Missing data in a sample is in this shape:     ./.
## + Masked data has the genotype "set to unknown": ./.:42:5,0:33
## + Present data has the shape:                    0/1:42:5,0:33
## + The separator of the Genotypes (GT) are '/' for unphased; '|' for phased genotypes


## SUPPLEMENTARY ONGOING TASKS

## [] Plan to read gz
##    Chose:  [-] re-code all the function `loci_stuff` with b'str'
##            [+] Put the main part of the script into another function and decode each line as input to `loci_stuff`
##
##    Requirements: import gzip
## - Functions for opening and decoding are coded. The samp_n[miss|mask] are empty and len==1
## - Working on the function is_file_gz to get the headers in case of GZ

## Future tasks:
## 
##     - open GZ
##     - Handle output names from options (like plink or VCFtools)
##     - Count the REAL number of genotypes: .|.;  .|.|.; etc 


###########################3


import sys
import argparse


def is_file_gz(file):
    '''
    Test if file is gz or not
    If file is not binary it will return a list of samples
    '''

    is_binary = False
    samp_li = []

    try:
        handle = open(file, 'r')
        ## handle.readline() fails when reading a binary file

        l = handle.readline()
        print("Your file is an uncompressed file.")

        for line in handle:
            
            ## The line of ^#C being at the end of the preambule of the VCF
            ## This will create and write the adequate header_imiss in this place.

            if line.startswith('#C'):
                
                samp_li = line.strip()
                samp_li = samp_li.split('\t')
                samp_li = samp_li[9:]
                handle.close()

                ## Return stops the loop
                return is_binary, samp_li


    except UnicodeDecodeError:

        is_binary = True

        import gzip


        handle = gzip.open(file, 'r')

        l = handle.readline()
        print("Your file may be compressed.")

        for line in handle:
            
            ## The line of ^#C being at the end of the preambule of the VCF
            ## This will create and write the adequate header_imiss in this place.

            if line.startswith(b'#C'):
                

                samp_li = line.decode()
                samp_li = samp_li.strip()
                samp_li = samp_li.split('\t')
                samp_li = samp_li[9:]
                handle.close()

                ## Return stops the loop
                return is_binary, samp_li

def only_GT(sample):
    '''
    If the information for each sample contains only the genotype,
    We cannot distinguish the masked and missing
    '''
    if len(sample) == 1:
        return TRUE

def loci_stuff(locus):
    '''
    Count missing and masked on one line:
    '''
    split_line = locus.split('\t')
    nb_samples = len(split_line) - 9
 
    ## Counts for loci (by line)
    l_n_data, l_n_miss, l_n_mask = 0, 0, 0

    ## Counts for samples(by columns)
    s_n_data = 0  # sync with l_n_data
    s_n_miss, s_n_mask = [0]*nb_samples, [0]*nb_samples

    ## Parse the list of sample info skipping the first mandatory columns from the VCF
    for sample in split_line[9:]:

        sample = sample.split(':')

        ## Are there only genotypes ? No AD, GL or alike per sample, see FORMAT in the VCF
        ## If True: masked can't be distinguished from missing
        if only_GT(sample) == True:
            print('The sample number:', l_n_data, 'contains only genotype (GT) information.')
            print('It is impossible to distinguish between masked and missing. Other tools already do this')
            print('No M&Ms for you.' )
            sys.exit(1)


        ####
        ## This is the place to count the real number of genotypes independently of ploidy
        ## sample[0] is the first element of the information found per individual.
        ## The separator is | or / if genotypes are phased or unphased.
        ## Instructions: split by those separators and count the length of that list = nb of genotypes for this sample
        ## When this will be implemented verify that the frequency is the same (N_MISS/N_DATA)
        ####

        ## If the firts characters of the first two elements are a '.' == missing data
        if sample[0][0] == '.':
            if sample[1][0] == '.':
                l_n_miss += 1
                s_n_miss[l_n_data] += 1
            ## If the 1st char of the 1st element == '.', and 1st char of 2nd element =! '.' == masked
            else:
                l_n_mask += 1
                s_n_mask[l_n_data] += 1

        ## +1 counters for each sample
        l_n_data += 1
        s_n_data += 1   # Redundant, may disapear

    f_miss = round(l_n_miss/l_n_data, 6)
    f_mask = round(l_n_mask/l_n_data, 6)

    # Expected output line:
    # CHR     POS     N_DATA  N_GENOTYPE_FILTERED     N_MISS  F_MISS    N_MASK  F_MASK
    line_out =  split_line[0] + '\t' + \
                split_line[1] + '\t' + \
                str(l_n_data) + '\t' + \
                str(0) + '\t' + \
                str(l_n_miss) + '\t' + \
                str(f_miss) + '\t' + \
                str(l_n_mask) + '\t' + \
                str(f_mask)  + '\n'


    return line_out, s_n_miss, s_n_mask

def do_the_thing_str(infile_vcf, outfile_lmiss, ind_samp_names):
    '''
    Opens input file, strips empty trailing spaces,
    Feeds line into the function `loci_stuff`
    Writes `output.lmiss` as vcf is treated line by line
    Returns: samp_ndata, samp_nmiss, samp_nmask
        - 
    '''
    with open(infile_vcf, 'r') as handle, open(outfile_lmiss, 'w') as h_lmiss :

        ## Create counts for columns lists. len == len(ind_samp_names)
        ind_samp_nmiss, ind_samp_nmask = [0]*len(ind_samp_names), [0]*len(ind_samp_names)
        ind_samp_ndata = 0

        for line in handle:
            li = line.strip()       ## strips leading empty spaces

            if not li.startswith('#'):
                
                line_lmiss, samp_nmiss_line, samp_nmask_line = loci_stuff(li)            # only_genotypes = loci_stuff(li)
                h_lmiss.write(line_lmiss)


                ## Make sum of list per line with cumulated lists
                ind_samp_nmiss = [i+j for i,j in zip(ind_samp_nmiss, samp_nmiss_line)]
                ind_samp_nmask = [i+j for i,j in zip(ind_samp_nmask, samp_nmask_line)]
                ind_samp_ndata +=1


            elif li.startswith('#C'):

                ## The line of ^#C being at the end of the preambule of the VCF,
                ## occurs before the data and occurs only once,
                ## It's the right time to write the header .lmiss

                header_lmiss = ['CHR', 'POS', 'N_DATA', 'N_GENOTYPE_FILTERED', 'N_MISS', 'F_MISS', 'N_MASK', 'F_MASK']
                header_lmiss = '\t'.join(header_lmiss) + '\n'

                h_lmiss.write(header_lmiss)
    return ind_samp_ndata, ind_samp_nmiss, ind_samp_nmask

def do_the_thing_gz(infile_vcf, outfile_lmiss, ind_samp_names):
    '''
    Requires to import gzip
    Opens input file, strips empty trailing spaces,
    Feeds line into the function `loci_stuff`
    Writes `output.lmiss` as vcf is treated line by line
    Returns: samp_ndata, samp_nmiss, samp_nmask
        - 
    '''

    import gzip

    with gzip.open(infile_vcf, 'r') as handle, open(outfile_lmiss, 'w') as h_lmiss :

        ## Create counts for columns lists. len == len(ind_samp_names)
        ind_samp_nmiss, ind_samp_nmask = [0]*len(ind_samp_names), [0]*len(ind_samp_names)
        ind_samp_ndata = 0

        for line in handle:
            li = line.strip()       ## strips leading empty spaces

            if not li.startswith(b'#'):
                
                ## Decode line. `loci_stuff` works with str
                line_lmiss, samp_nmiss_line, samp_nmask_line = loci_stuff(li.decode())            # only_genotypes = loci_stuff(li)
                h_lmiss.write(line_lmiss)


                ## Make sum of list per line with cumulated lists
                ind_samp_nmiss = [i+j for i,j in zip(ind_samp_nmiss, samp_nmiss_line)]
                ind_samp_nmask = [i+j for i,j in zip(ind_samp_nmask, samp_nmask_line)]
                ind_samp_ndata +=1


            elif li.startswith(b'#C'):

                ## The line of ^#C being at the end of the preambule of the VCF,
                ## occurs before the data and occurs only once,
                ## It's the right time to write the header .lmiss

                header_lmiss = ['CHR', 'POS', 'N_DATA', 'N_GENOTYPE_FILTERED', 'N_MISS', 'F_MISS', 'N_MASK', 'F_MASK']
                header_lmiss = '\t'.join(header_lmiss) + '\n'

                h_lmiss.write(header_lmiss)

    return ind_samp_ndata, ind_samp_nmiss, ind_samp_nmask


############## Run the program with functions

parser = argparse.ArgumentParser(
    prog='m_and_m_counter',
    description='Count Masked and Missing data from VCF files',
    epilog="Peace love and rock'n roll\n")
parser.add_argument('-v', '--vcf',
    type=str, 
    help="Input VCF file")

args=parser.parse_args()

# parser.print_help()

print("Input file:", args.vcf)


### OUTPUT FILE NAMES
### Outputs created in the same dir as input file
of_lmiss = args.vcf.split('.')[0] + '.lmiss'
of_imiss = args.vcf.split('.')[0] + '.imiss'



## Test if file is binary, if false get the list of samples

is_file_binary, samp_names = is_file_gz(args.vcf)

## Print some info for the user
if len(samp_names) == 0:
    print("The file is probably wrongly formated. There are no samples")
    sys.exit(1)
else:
    print("File contains ", len(samp_names), " samples.")


## Analyse text files
if is_file_binary == False:
    samp_ndata, samp_nmiss, samp_nmask =  do_the_thing_str(args.vcf, of_lmiss, samp_names)
else:
    samp_ndata, samp_nmiss, samp_nmask =  do_the_thing_gz(args.vcf, of_lmiss, samp_names)



## Write imiss

with open(of_imiss, 'w') as h_imiss:
    
    ## Write header

    header_imiss = ['INDV', 'N_DATA', 'N_GENOTYPES_FILTERED', 'N_MISS', 'F_MISS', 'N_MASK', 'F_MASK']
    header_imiss = '\t'.join(header_imiss) + '\n'
    h_imiss.write(header_imiss)

    for i,j,k in zip(samp_names, samp_nmiss, samp_nmask):

    ## Expected output line:
    ## INDV    N_DATA  N_GENOTYPES_FILTERED    N_MISS  F_MISS   N_MASK   F_MASK

        line_out = i + '\t' + \
                    str(samp_ndata) + '\t' + \
                    str(0) + '\t' + \
                    str(j) + '\t' + \
                    str(round(j/samp_ndata,6)) + '\t' + \
                    str(k) + '\t' + \
                    str(round(k/samp_ndata,6)) + '\n' \

        h_imiss.write(line_out)
