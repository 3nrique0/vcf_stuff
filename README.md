# VCF_stuff

Tool(s?) to exploit information from VCF files.


## Masked and missing counter

Script made with regular libraries of python3, no pandas, no matplotlib.
It counts the "Missingness" of data in a VCF file.
VCFtools already does this job, however it cannot distinguish between masked and missing data.

Sometimes masking samples under different criteria may become helpful.
Masking data means to convert the GT (genotype) information to '.':

* Missing data looks looks like this:			 ./.:.:.,.:.
* Masked data has the genotype "set to unknown": ./.:42:5,0:33
* Present data has the shape:                    0/1:42:5,0:33

If the only information present in the VCF file are only genotypes, there is no way to distinguish between masked and missing, in this case the program will stop

There are 2 outputs, shaped as those from VCFtools, but with 2 extra columns N_MASK and F_MASK. Both counts are produced at once:

* imiss - The missingness in individuals (samples) = by columns
* lmiss - The missingness by locus = by lines

The extra columns N_MASK and F_MASK, contain the information of the counts of masked data, and the frequency.

More details are present in the comments in the script.

The input files are VCF formated files, it automatically detects and can handle text and gzip files

### Usage:


```bash
## Get help
python3 masked_missing_counter.py -h

## Run the program
python3 masked_missing_counter.py --vcf sample.vcf[.gz]
```

The outputfiles will be located in the same directory as the input file and the extention .vcf or .vcf.gz will be .imiss and .lmiss


## Support
Use the issue tracker please.

## Roadmap
Counting the roadmap is written in the comments of the script.

## Contributing
Open to contributions.
